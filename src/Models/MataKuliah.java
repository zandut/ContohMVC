/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author ZANDUT
 */
public class MataKuliah
{
    private int idMatkul;
    private String namaMatkul;

    public MataKuliah()
    {
    }

    public MataKuliah(int idMatkul, String namaMatkul)
    {
        this.idMatkul = idMatkul;
        this.namaMatkul = namaMatkul;
    }

    public int getIdMatkul()
    {
        return idMatkul;
    }

    public void setIdMatkul(int idMatkul)
    {
        this.idMatkul = idMatkul;
    }

    public String getNamaMatkul()
    {
        return namaMatkul;
    }

    public void setNamaMatkul(String namaMatkul)
    {
        this.namaMatkul = namaMatkul;
    }
    
    
}
