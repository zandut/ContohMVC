/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;

/**
 *
 * @author ZANDUT
 */
public class Mahasiswa
{
    private String nim;
    private String nama;
    private ArrayList<MataKuliah> arrayMatkul = new ArrayList<>();

    public Mahasiswa(String nim, String nama)
    {
        this.nim = nim;
        this.nama = nama;
    }

    public Mahasiswa()
    {
    }
    
    public void addMatkul(MataKuliah matkul)
    {
        arrayMatkul.add(matkul);
    }

    public ArrayList<MataKuliah> getArrayMatkul()
    {
        return arrayMatkul;
    }
    
    

    public String getNim()
    {
        return nim;
    }

    public void setNim(String nim)
    {
        this.nim = nim;
    }

    public String getNama()
    {
        return nama;
    }

    public void setNama(String nama)
    {
        this.nama = nama;
    }
    
    
}
