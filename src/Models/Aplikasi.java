/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author ZANDUT
 */
public class Aplikasi
{
    private Database db;

    public Aplikasi()
    {
        db = new Database();
    }
    
    public boolean saveMahasiswa(Mahasiswa m)
    {
        db.connect();
        boolean berhasil = db.manipulate("insert into mahasiswa (nim, nama) values ('"+m.getNim()+"','"+m.getNama()+"');");
        db.disconnect();
        return berhasil;
    }
    
    public Mahasiswa getMahasiswa(long nim)
    {
        db.connect();
        Mahasiswa mahasiswa = new Mahasiswa();
        ResultSet rs = db.getData("select * from mahasiswa where nim='"+nim+"'");
        try
        {
            if (rs.first())
            {
                mahasiswa.setNim(rs.getString("nim"));
                mahasiswa.setNama(rs.getString("nama"));
                
                ResultSet rsMatkul = db.getData("select * from mata_kuliah where nim='"+nim+"'");
                while (rsMatkul.next())
                {
                    MataKuliah matkul = new MataKuliah(rsMatkul.getInt("id_matkul"),
                            rsMatkul.getString("nama_matkul"));
                    mahasiswa.addMatkul(matkul);
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        db.disconnect();
        return mahasiswa;
    }
    
    public ArrayList<Mahasiswa> getAllMahasiswa()
    {
        db.connect();
        ArrayList<Mahasiswa> arrayMahasiswa = new ArrayList<>();
        ResultSet rs = db.getData("select * from mahasiswa");
        try
        {
            while (rs.next())
            {
                Mahasiswa m = new Mahasiswa(rs.getString("nim"), rs.getString("nama"));
                arrayMahasiswa.add(m);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        
        db.disconnect();
        return arrayMahasiswa;
        
    }
    
    
    
}
