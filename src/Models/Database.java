/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ZANDUT
 */
public class Database
{

    private Connection connection;
    private Statement statement;

    public void connect()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/db_tp", "root", "");
            statement = connection.createStatement();
        } catch (Exception ex)
        {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void disconnect()
    {
        try
        {
            connection.close();
            statement.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public boolean manipulate(String query)
    {
        boolean berhasil = false;
        try
        {
            int rows = statement.executeUpdate(query);
            if (rows > 0)
            {
                berhasil = true;
            }
            
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return berhasil;
    }
    
    public ResultSet getData(String query)
    {
        ResultSet rs = null;
        try
        {
            rs = statement.executeQuery(query);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return rs;
    }

}
