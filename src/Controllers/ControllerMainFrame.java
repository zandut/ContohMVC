/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Aplikasi;
import Models.Mahasiswa;
import Views.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ZANDUT
 */
public class ControllerMainFrame implements ActionListener
{

    private MainFrame frame;
    private Aplikasi apps;

    public ControllerMainFrame()
    {
        apps = new Aplikasi();
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        
        frame = new MainFrame();
        RefreshTable(apps.getAllMahasiswa(), frame.getTableMahasiswa());
        
        frame.getBtnSimpan().addActionListener(this);
        
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    
    private void RefreshTable(ArrayList<Mahasiswa> data, JTable table)
    {
        DefaultTableModel tb = (DefaultTableModel) table.getModel();
        tb.setRowCount(0);
        for (Mahasiswa mhs : data)
        {
            String[] dataTable = {mhs.getNim(), mhs.getNama()};
            tb.addRow(dataTable);
        }
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource().equals(frame.getBtnSimpan()))
        {
            Mahasiswa m = new Mahasiswa(frame.getTextNIM().getText(), frame.getTextNama().getText());
            boolean berhasil = apps.saveMahasiswa(m);
            if (berhasil)
            {
                frame.showMessage("Mahasiswa "+m.getNama()+" berhasil disimpan");
                RefreshTable(apps.getAllMahasiswa(), frame.getTableMahasiswa());
            }
        }
    }
    
}
