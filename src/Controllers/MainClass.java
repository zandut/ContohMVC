/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Aplikasi;
import Models.Mahasiswa;
import Models.MataKuliah;

/**
 *
 * @author ZANDUT
 */
public class MainClass
{
    public static void main(String[] args)
    {
        Aplikasi apps = new Aplikasi();
        Mahasiswa mhs = apps.getMahasiswa(1301158599);
        System.out.println("nama : "+mhs.getNama());
        for (MataKuliah value : mhs.getArrayMatkul())
        {
            System.out.println("matkul : "+value.getNamaMatkul());
        }
    }
}
